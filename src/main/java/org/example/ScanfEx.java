package org.example;

import java.util.Scanner;

public class ScanfEx {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 문자열을 숫자로 변경할때 Integer.parseInt로 해줘야한다.
        String c = scanner.nextLine();

        int a = Integer.parseInt(c);
        int b = (int) Double.parseDouble(c);

        System.out.println(a);
        System.out.println(b);
    }
}
