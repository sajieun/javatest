package org.example;

public class VarEx3 {
    public static void main(String[] args) {
        final int score = 100;
        byte b = 127; // -128 ~ 127
        boolean power = true;

        char ch = 'A';  //A출력 '' 이렇게만 하면 오류남
        int a = 'A';    // 65출력

        String str = ""; // 얘는 빈문자 가능
        System.out.println(power);

        int x = 10, y = 20;
        int tmp;
        tmp = x;
        x=y;
        y = tmp;

        System.out.println(x);
        System.out.println(y);
    }
}
