package org.example;

public class PrintfEx1 {
    public static void main(String[] args) {
//        System.out.println(10.0/3);

        System.out.printf("%d%n",15);
        System.out.printf("%#o%n",15);
        System.out.printf("%#x%n",15);


        System.out.printf("[%5d]%n",15);
        System.out.printf("[%-5d]%n",15);
        System.out.printf("[%05d]%n",15);


    }
}
